<?php

namespace Drupal\Tests\images_optimizer\Unit\HookHandler;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\images_optimizer\HookHandler\HelpHookHandler;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Unit test class for the HelpHookHandler class.
 *
 * @package Drupal\Tests\images_optimizer\Unit\HookHandler
 */
class HelpHookHandlerTest extends UnitTestCase {


  /**
   * @var (ExtensionPathResolver&MockObject)|NULL
   */
  private $extensionPathResolver = NULL;

  /**
   * The help hook handler to test.
   *
   * @var \Drupal\images_optimizer\HookHandler\HelpHookHandler
   */
  private $helpHookHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    if (class_exists(ExtensionPathResolver::class)) {
      $this->extensionPathResolver = $this->createMock(ExtensionPathResolver::class);
    }

    $this->helpHookHandler = new HelpHookHandler($this->extensionPathResolver);
  }

  /**
   * Test process() with an unrelated route name.
   */
  public function testProcessWithAnUnrelatedRouteName() {
    $this->assertNull($this->helpHookHandler->process('foo'));
  }

}
