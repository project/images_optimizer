<?php

namespace Drupal\images_optimizer\HookHandler;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hook handler for the help() hook.
 *
 * @package Drupal\images_optimizer\HookHandler
 */
class HelpHookHandler implements ContainerInjectionInterface {

  /** @var ExtensionPathResolver|NULL  */
  private $extensionPathResolver = NULL;

  public function __construct(?ExtensionPathResolver $extensionPathResolver = NULL) {
    $this->extensionPathResolver = $extensionPathResolver;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->has('extension.path.resolver') ? $container->get('extension.path.resolver') : NULL
    );
  }

  /**
   * Get the content for the help page of our module.
   *
   * @param string $route_name
   *   The route name.
   *
   * @return \Drupal\Component\Render\MarkupInterface|null
   *   The markup or NULL.
   */
  public function process($route_name) {
    // @TODO: Provides other useful information instead?
    // Or use Markdown Filter module if it is installed?
    if (version_compare(\Drupal::VERSION, '9.3.0', '<')) {
      return $route_name === 'help.page.images_optimizer' ?
        check_markup(file_get_contents(drupal_get_path('module', 'images_optimizer') . '/README.txt')) : NULL;
    }
    else {
      return $route_name === 'help.page.images_optimizer' ?
        check_markup(file_get_contents($this->extensionPathResolver->getPath('module', 'images_optimizer') . '/README.txt')) : NULL;
    }
  }

}
